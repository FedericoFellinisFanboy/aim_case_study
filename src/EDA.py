# %% Convert the large xlsx file into a faster csv file - this takes some time..!
import dask
import pandas as pd
import numpy as np
import dask.dataframe as ddf
from dask.delayed import delayed
from sklearn.utils import axis0_safe_slice

convert_xlsx_to_csv = False

if convert_xlsx_to_csv:
    parts = dask.delayed(pd.read_excel)("Example_MLE.xlsx")
    df_per_dask = ddf.from_delayed(parts)
    df_per_dask.to_csv("Example_MLE.csv", single_file = True)

# %% Load the data
df_raw = pd.read_csv("Example_MLE.csv",
                        index_col = 0,
                        low_memory= False
                    )
df_raw
# %% Split the data into BOLD BIN part and the habitat data

column_names = df_raw.keys()
habitat_types = ["agricultural", "Forest", "Grassland", "Urban"]

habitat_columns = [col for col in column_names for habitat_type in habitat_types if habitat_type in col]
non_habitat_columns = [col for col in column_names if not col in habitat_columns]

df_BOLD_raw = pd.DataFrame(df_raw.loc[4:,non_habitat_columns]).copy().fillna("")
df_BOLD_raw.rename(columns = {old_col_name : new_col_name for old_col_name, new_col_name in zip(non_habitat_columns,df_raw.loc[3,non_habitat_columns])}, inplace=True)
df_habitat_raw = df_raw[habitat_columns].fillna(0)

# %% Make dataframe BOLD nicer:
# - shorten the column "BOLD_BIN_uri"
# - split and shorten the column "OTU_ID;cluster_size"
# - add column "sum_reads_per_OTU"

df_BOLD_all = df_BOLD_raw.drop(["BOLD_BIN_uri", "OTU_ID;cluster_size"], axis = 1)

df_BOLD_all["BOLD_BIN_uri"] = [str(bin.replace("BOLD:", "")) for bin in  df_BOLD_raw['BOLD_BIN_uri']]
df_BOLD_all[["OTU_ID", "cluster_size"]] = df_BOLD_raw["OTU_ID;cluster_size"].str.split(";",expand=True,)
df_BOLD_all["OTU_ID"] = [id.replace("OTU", "") for id in  df_BOLD_all['OTU_ID']]
df_BOLD_all["cluster_size"] = [int(id.replace("size=", "")) for id in  df_BOLD_all['cluster_size']]
df_BOLD_all["sum_reads_per_OTU"] = df_raw.loc[4:, "habitat type"].astype(int)

# %% Make dataframe BOLD smaller:
# - delete all rows without an 'BOLD_BIN_uri'
# - delete all rows with 'sum_read_per_OTU' below a margin
# - delete all rows with 'consensus score' C

mask = (
    (df_BOLD_all["BOLD_BIN_uri"] != '')
    #& (df_BOLD_all["sum_reads_per_OTU"] > 10)
    #& (df_BOLD_all["consensus score"] != 'C')
    )
df_BOLD = df_BOLD_all[mask].copy()

# %%
df_BOLD
# %% Make dataframe habitat nicer:
# - transpose it
# - rename columns as BIN features
# - create target variable "habitat_date"

df_habitat = df_habitat_raw.T.reset_index()

new_habitat_columns = {
    "index" : "habitat_type",
    0 : "lat",
    1 : "lon",
    2 : "date",
    3 : "ID"
}
features = [df_BOLD.loc[i, 'BOLD_BIN_uri'] if i in df_BOLD.index else '' for i in range(4,len(df_raw))]
new_habitat_columns.update({old_col_name : new_col_name for old_col_name, new_col_name in zip(range(4, len(df_raw)), features)})
# %%
df_habitat.rename(columns = new_habitat_columns, inplace=True)
df_habitat.drop(columns = [''], axis = 1, inplace = True)
df_habitat["habitat_type"] = [habitat_type for habitat in df_habitat["habitat_type"] for habitat_type in habitat_types if habitat_type in habitat]
df_habitat["date"] = df_habitat["date"].str[:3]
df_habitat["target"] = df_habitat["date"] + "_" + df_habitat["habitat_type"]

# %% Set up dataframe for training
X = df_habitat.drop(columns = ["habitat_type","lat","lon","date","ID", "target"], axis = 1).astype(int)

#X[X != 0] = 1
#(X == 0).sum(axis = 1).sum(axis=0) / (np.prod(X.shape))
y = df_habitat["target"]

# # %% Dimensionality reduction
# from sklearn.decomposition import TruncatedSVD

# svd = TruncatedSVD(random_state=42)
# X_svd = svd.fit_transform(X)

# # %%
# from sklearn.decomposition import PCA

# pca = PCA(n_components=2)
# X_pca = pca.fit_transform(X)

# %%
# import plotly.express as plx

# fig = plx.scatter(x = X_svd[:,0], y = X_svd[:,1],
#     #facet_col = df_habitat["habitat_type"],
#     color = df_habitat["date"]
# )
# fig.show()

# # %%
# fig = plx.scatter(x = X_pca[:,0], y = X_pca[:,1],
#     facet_col = df_habitat["habitat_type"],
#     color = df_habitat["date"],
#     title = "Dimension Reduction to Visualize Separatation of the Months"
# )
# fig.update_xaxes(showline=False)
# fig.show()


# %%
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.25, random_state=42, stratify = y)


X_train.shape
# %%
# from sklearn.preprocessing import StandardScaler
# scaler = StandardScaler()

# X_train = scaler.fit_transform(X_train)
# X_test = scaler.transform(X_test)

# %%
from imblearn.over_sampling import SMOTE

oversample = SMOTE()
X_train, y_train = oversample.fit_resample(X_train, y_train)


# %%
from sklearn.ensemble import RandomForestClassifier

clf = RandomForestClassifier(max_depth=2, random_state=0)
clf.fit(X_train, y_train)

y_predict = clf.predict(X_test)
# %%
from sklearn.metrics import confusion_matrix, classification_report

conf_matrix = confusion_matrix(y_test, y_predict)
cl_report = classification_report(y_test, y_predict)
# %%
import seaborn as sns
import matplotlib.pyplot as plt

sns.heatmap(conf_matrix, annot=True)
# %%
print(cl_report)

# %%
Next: PCA (features -> 50) + SMOTE + Scaler + Weights + gridsearch (+ LIME)
# %%

Further possibilities:

Feature engineering
- "BOLD_BIN_uri" duplicates -> sum them up(?)
- Some "BOLD_BIN_uri" are very rare (e.g. AAH8078 = 5 / 2109 observations)