# %%
print("Get the BINs for prediction from 'BOLD_BIN_uri_for_prediction.csv' and their counts (if available) from 'BOLD_BIN_uri_counts_for_prediction.csv'")
print("Write the predictions as likelihoods into 'Predictions_single_Results.csv'")

# # %%
import pickle

filename = "random_forest_model.sav"
clf = pickle.load(open(filename, 'rb'))

filename = "random_forest_dummy_model.sav"
clf_dummy = pickle.load(open(filename, 'rb'))

# %%
import pandas as pd
import numpy as np

df_test_raw = pd.read_csv("BOLD_BIN_uri_for_prediction.csv", header=None)

try:
    df_counts = pd.read_csv("BOLD_BIN_uri_counts_for_prediction.csv", header=None)
    df_test_raw[1] = pd.read_csv("BOLD_BIN_uri_counts_for_prediction.csv", header=None)
except:
    pass


df_test = df_test_raw.T
df_test.columns = df_test.iloc[0].str.replace("BOLD:", "")
df_test = df_test[1:]

# %%
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()

X_train = pd.read_csv("X_train.csv", index_col=0)
scaler.fit_transform(X_train)


# %%
if df_test.shape[0] > 0:

    X_test = pd.DataFrame(0, index=df_test.index, columns = X_train.keys())

    for col in df_test.keys():
        if col in X_train.keys():
            X_test[col] = df_test[col]

    X_test = scaler.transform(X_test)

    y_predict = pd.DataFrame(clf.predict_proba(X_test), columns = clf_dummy.classes_, index = None)
    y_predict.to_csv("Predictions_single_Results.csv")

else:

    X_test = pd.DataFrame(0, index=[0], columns = X_train.keys())

    for col in df_test.keys():
        X_test[col] = 1

    y_predict = pd.DataFrame(clf_dummy.predict_proba(X_test), columns = clf_dummy.classes_, index = None)
    y_predict.to_csv("Predictions_single_Results.csv")
